/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package codejam2009;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author kitinon
 */
public class AlienLanguage {

    private static ArrayList matchAt(ArrayList dict, char[] cs, int pos) {
        ArrayList result = new ArrayList();
        for (Object word : dict) {
            for (char c: cs) {
                if (((String)word).toCharArray()[pos] == c) {
                    result.add(word);
                }
            }
        }
        return result;
    }

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException{
        Scanner in;
        if (Array.getLength(args) > 0) {
            in = new Scanner(new BufferedReader(new FileReader(args[0])));
        } else {
            in = new Scanner(System.in);
        }
        int L = in.nextInt();
        int D = in.nextInt();
        int N = in.nextInt();
        in.nextLine();
        
        ArrayList dict = new ArrayList();
        for (int i=0; i<D; i++) dict.add(in.nextLine());

        for (int i=1; i<=N; i++) {
            String word = in.nextLine();
            ArrayList d = dict;
            int pos = 0;
            boolean intoken = false;
            String token = "";
            for (char c: word.toCharArray()) {
                switch (c) {
                    case '(' :
                        intoken = true;
                        break;
                    case ')' :
                        intoken = false;
                        d = matchAt(d, token.toCharArray(), pos++);
                        token = "";
                        break;
                    default:
                        if (intoken) {
                            token += c;
                        } else {
                            char[] cs = {c};
                            d = matchAt(d, cs, pos++);
                        }
                        break;
                }
            }
            System.out.println("Case #" + i + ": " + d.size());
        }
    }
}